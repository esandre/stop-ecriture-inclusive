const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const firefox = require('./webpack.firefox.js');
const chrome = require('./webpack.chrome.js');
const path = require('path');

var prod = {
  mode: 'production',
  devtool: 'source-map'
};

module.exports = [
  merge(common, firefox),
  merge(common, chrome)
];
