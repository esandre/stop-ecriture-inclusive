# stop-ecriture-inclusive

Une extension disponible sur Firefox et Chrome pour corriger l’écriture inclusive d’une page web en temps réel.

## Français
## Détails techniques

Insaller les dépendances
```bash
 npm install
```

Construire l’extension avec Webpack
Les fichiers pour Firefox seront dans **/dist/firefox**
Les fichiers pour Chrome seront dans **/dist/chrome**
```bash
npm run build
```

Packager l’extension dans le but de la distribuer ou de la publier pour FIREFOX
Le package zip sera dans **/builds/web-ext-articfacts/**
```bash
npm run package-firefox
```

Packager l’extension dans le but de la distribuer ou de la publier pour CHROME
Le package zip sera dans **/builds/**
```bash
npm run package-chrome
```

Construire l’extension dans le but de la tester localement avec le live reload :
```bash
npm run watch
```

Sur Firefox, vous pouvez tester l’application en lançant la commande suivante depuis le dossier **/dist/firefox**  
Plus d’informations sur https://extensionworkshop.com/documentation/develop/web-ext-command-reference/
```bash
web-ext run --verbose
```

Nettoyer le dossier dist :
```bash
npm run clean-dist
```

## English
## Technical Details

Install dependencies
```bash
npm install
```

Build the extension with Webpack
Files for Firefox will be in **/dist/firefox**
Files for Chrome will be in **/dist/chrome**
```bash
npm run build
```

Package the extension in order to publish and/or distribute for FIREFOX
Le package zip sera dans **/builds/web-ext-articfacts/**
```bash
npm run package-firefox
```

Package the extension in order to publish and/or distribute for CHROME
Le package zip sera dans **/builds/**
```bash
npm run package-chrome
```

Build the extension to test it locally with live reload:
```bash
npm run watch
```

On Firefox, you can test the application using the following command from the folder **/dist/firefox**  
More infos at https://extensionworkshop.com/documentation/develop/web-ext-command-reference/
```bash
web-ext run --verbose
```

Clean the dist folder :
```bash
npm run clean-dist
```
