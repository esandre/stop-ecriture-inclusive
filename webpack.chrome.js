const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  name: "chrome",
  entry: {
    chrome_content_script: __dirname + '/src/chrome/chrome-content-script.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/chrome/')
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: 'chrome-manifest.json', to: 'manifest.json'},
        {from: 'icons/png', to: 'icons/'}
      ],
    })
  ]
};
