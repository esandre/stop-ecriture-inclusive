const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { merge } = require('webpack-merge');
const path = require('path');

module.exports = {
  entry: {
    content_script: __dirname + '/src/content-script.js',
    background_script: __dirname + '/src/background-script.js',
    popup_script: __dirname + '/src/popup/popup-script.js'
  },
  resolve: {
    modules: ['./src', './node_modules'],
    alias: {
      XRegExp: 'XRegExp'
    }
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 11,
          output: {
            ascii_only: true
          }
        }
      })
    ]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: 'node_modules/webextension-polyfill/dist/browser-polyfill.js'},
        {from: 'src/popup/popup.html'},
        {from: 'src/popup/popup-style.css'},
      ],
    })
  ]
};
