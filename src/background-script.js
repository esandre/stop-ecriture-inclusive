import XRegExp from 'xregexp';

/**
 * @method isExtensionEnabled
 * @return {Boolean}
 */
function isExtensionEnabled() {
  console.log('isExtensionEnabled: ', localStorage.getItem('extensionStatus') === 'enabled');
  return localStorage.getItem('extensionStatus') === 'enabled';
}

/**
 * @method setExtensionStatus
 * @param  {String}           extensionStatus either 'enabled' or 'disabled'
 * @return {undefined}
 */
function setExtensionStatus(extensionStatus) {
  return localStorage.setItem('extensionStatus', extensionStatus);
}

setExtensionStatus('enabled');

/**
 * @method setDebugOptions
 * @param  {Object}        debugOpions
 * @return {Object}
 */
function setDebugOptions(debugOpions) {
  localStorage.setItem('debugOpions', JSON.stringify(debugOpions));
  return DEBUG = debugOpions;
}

/**
 * @method getDebugOptions
 * @return {Object|undefined}
 */
function getDebugOptions() {
  return JSON.parse(localStorage.getItem('debugOpions'));
}

var DEBUG = getDebugOptions() ?? setDebugOptions({ log: false, timer: false });

/**
 * Set browserAction icon depending on the browser as Chrome doesn't support SVG
 * @method setBrowserActionIcon
 * @param  {Boolean}            isEnabled
 */
function setBrowserActionIcon(isEnabled) {
  let iconToUse = isEnabled ? 'stop-ecriture-inclusive' : 'stop-ecriture-inclusive-bw';
  // window.navigator.vendor will be empty on Firefox,
  // see https://developer.mozilla.org/en-US/docs/Web/API/Navigator/vendor
  iconToUse += window.navigator.vendor.length ? '.png' : '.svg';
  browser.browserAction.setIcon({path: `icons/${iconToUse}`});
}

/**
 * generate the regexps array and their replace word
 * @method generateReplaceEachRegexpsArray
 * @return {array}             An array of regexps and their replace word
 */
function generateReplaceEachRegexpsArray() {
  const wordFilters = {
    mien: ['miem', 'miæn'],
    tous: ['touste', 'toustes', 'touxe', 'touxes'],
    celui: ['cellui', 'célui'],
    ceux: ['celleux', 'ceulles', 'ceuxes', 'ceuze'],
    eux: ['elleux', 'euxes', /*'euze', 'eus'*/],
    //mon: ['maon'],
    //ton: [/*'tan',*/ 'taon'],
    //son: ['saon'],
    ce: ['cès', 'cèx'],
    un: [/*'um',*/ 'unæ'],
    //du: ['di'],
    //le: ['lia'/*, 'li', 'lo'*/],
    ils: ['illes', 'ils\\.elles', 'ils·elles', 'elles\\.ils', 'elles·ils', 'il\\.elle\\.s', 'il·elle·s', 'elle\\.il\\.s', 'elle·il·s', /*'uls', 'ols', 'yels',*/ 'iels', 'ielles', /*'ims', 'iems', 'euls', 'yas'*/],
    il: ['ille', 'il\\.elle', 'il·elle', 'elle\\.il', 'elle·il', /*'ul', 'ol', 'yel',*/ 'iel', /*'el',*/ 'ielle', /*'im', 'iem', 'eul', 'ya', 'um', 'om', 'ax', 'ox'*/],
    lui: ['ellui']
  };

  let generatedWordsRegexp = [];
  const sBoundary = '(?<=^|\\P{Latin})';
  const eBoundary = '(?!-)(?=\\P{Latin}|$)';

  for(const [replaceWith, wordsToReplace] of Object.entries(wordFilters)) {
    let regexpStr = '';
    let i = 0;
    for(const word of wordsToReplace) {
      i++;
      regexpStr += sBoundary + word + eBoundary;
      regexpStr += i === wordsToReplace.length ? '' : '|';
    }
    generatedWordsRegexp[replaceWith] = XRegExp(regexpStr, 'mgiu');
  }

  return [
    [/s⋅e⋅s|s⋅es|s·e·s|s·es|s-e-s|s\.e\.s|s\.es/mgiu, 's'],
    [generatedWordsRegexp['mien'], 'mien'],
    [generatedWordsRegexp['tous'], 'tous'],
    [generatedWordsRegexp['celui'], 'celui'],
    [generatedWordsRegexp['ceux'], 'ceux'],
    [generatedWordsRegexp['eux'], 'eux'],
    // [generatedWordsRegexp['mon'], 'mon'],
    // [generatedWordsRegexp['ton'], 'ton'],
    // [generatedWordsRegexp['son'], 'son'],
    [generatedWordsRegexp['ce'], 'ce'],
    [generatedWordsRegexp['un'], 'un'],
    // [generatedWordsRegexp['du'], 'du'],
    // [generatedWordsRegexp['le'], 'le'],
    [generatedWordsRegexp['ils'], 'ils'],
    [generatedWordsRegexp['il'], 'il'],
    [generatedWordsRegexp['lui'], 'lui'],
    [XRegExp('(gentil(-|⋅|·|.)le|gentils(-|⋅|·|\\.)le)(-|⋅|·|\\.|)(?<!s|\\.(?=\\P{Latin}|$))', 'mgiu'), 'gentil'],
    [XRegExp('copaine', 'mgiu'), 'copain'],
    [/e-lle-au|e-au-lle|eau-elle|e⋅lle⋅au|e⋅au⋅lle|eau⋅elle|e·lle·au|e·au·lle|eau·elle|e\.lle\.au|e\.au\.lle|eau\.elle/mgiu, 'eau'],
    [/e-lles-aux|e-aux-lles|eaux-elles|e⋅lles⋅aux|e⋅aux⋅lles|eaux⋅elles|e·lles·aux|e·aux·lles|eaux·elles|e\.lles\.aux|e\.aux\.lles|eaux\.elles/mgiu, 'eaux'],
    [/-se-x|-ses-x|-x-se|-x-ses|⋅se⋅x|⋅ses⋅x|⋅x⋅se|⋅x⋅ses|·se·x|·ses·x|·x·se|·x·ses|\.se\.x|\.ses\.x|\.x\.se|\.x\.ses/mgiu, 'x'],
    [/-r-se|-se-r|-rs-se|-ses-r|⋅r⋅se|⋅se⋅r|⋅rs⋅se|⋅ses⋅r|·r·se|·se·r|·rs·se|·ses·r|\.r\.se|\.se\.r|\.rs\.se|\.ses\.r/mgiu, 'r'],
    [XRegExp('eurice(?=\\P{Latin}|s|$)', 'mgiu'), 'eur'],
    //Point médian
    [XRegExp('(?<=\\p{Latin})(·|⋅)[·⋅A-Za-zÀ-ÿ]*(?<!s)', 'mgiu'), ''],
    //Point
    [XRegExp('(?<!^|www)(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org)(?=\\P{Latin}|$))\\.(?=e\\.s|e(?=\\P{Latin}|$)|le|fe|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|le(?=\\P{Latin}|$)|te|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[.A-Za-zÀ-ÿ]*(?<!s|au\\W|x|\\.(?=\\P{Latin}|$))', 'mgiu'), ''],
    //Tiret
    [XRegExp('-(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org)(?=\\P{Latin}|$))(?=e-|e(?=\\P{Latin}|$)|fe-s(?=\\P{Latin}|$)|fe(?=\\P{Latin}|$)|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|te-s(?=\\P{Latin}|$)|te(?=\\P{Latin}|$)|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[-A-Za-zÀ-ÿ]*(?<!s|au\\W|x)', 'mgiu'), '']
  ];
}

const replaceEachRegexps = generateReplaceEachRegexpsArray();
DEBUG.log && console.log('replaceEachRegexps = ' + replaceEachRegexps);

browser.runtime.onMessage.addListener(
  (data, sender) => {
    DEBUG.log && console.log('resolving promise data:', data);
    switch (data.request) {
      case 'contentScriptData':
        return Promise.resolve(
          {
            replaceEachRegexps,
            isExtensionEnabled: isExtensionEnabled(),
            debugOpions: getDebugOptions()
          }
        );
        break;

      case 'popupData':
        return Promise.resolve(
          {
            isExtensionEnabled: isExtensionEnabled(),
            debugOpions: getDebugOptions()
          }
        );
        break;

      case 'toggleExtensionIsEnabled':
      console.log(data);
        setExtensionStatus(data.isEnabled ? 'enabled' : 'disabled');
        setBrowserActionIcon(data.isEnabled);
        browser.tabs.reload();
        break;

      case 'updateDebugOptions':
        setDebugOptions(data.debugOpions);
        browser.tabs.reload();
        break;
    }
  }
);
