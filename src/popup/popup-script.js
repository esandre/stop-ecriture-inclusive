var toggleExtensionElement = document.querySelector('#toggleExtension');
var debugCheckboxes = document.querySelectorAll('.debugCheckbox');

var DEBUG = { log: false, timer: false };
var extensionIsEnabled = true;

function getPopupDataFromBackground() {
  browser.runtime.sendMessage({
    request: 'popupData'
  }).then(
    async response => {
      isExtensionEnabled = await response.isExtensionEnabled;
      DEBUG = await response.debugOpions;
    },
    error => console.error('Error retrieving popupData from background:', error)
  ).then(
    () => {
      let log = document.querySelector('#debugLogsCheckbox');
      let timer = document.querySelector('#debugTimersCheckbox');
      log.checked = DEBUG.log;
      timer.checked = DEBUG.timer;
      toggleExtensionElement.textContent = isExtensionEnabled ? 'Désactiver l’extension' : 'Activer l’extension';
      extensionIsEnabled = isExtensionEnabled;
    }
  );
}

getPopupDataFromBackground();

toggleExtensionElement.onclick = e => {
  toggleExtensionElement.disabled = true;
  browser.runtime.sendMessage({
    request: 'toggleExtensionIsEnabled',
    isEnabled: !extensionIsEnabled
  }).then(
    () => {
      toggleExtensionElement.disabled = false;
      toggleExtensionElement.textContent = !extensionIsEnabled ? 'Désactiver l’extension' : 'Activer l’extension';
      extensionIsEnabled = !extensionIsEnabled;
    }
  ).then(getPopupDataFromBackground);
};

debugCheckboxes.forEach( checkbox => { checkbox.onclick = () => {
    let log = document.querySelector('#debugLogsCheckbox').checked;
    let timer = document.querySelector('#debugTimersCheckbox').checked;
    DEBUG = { log, timer };
    browser.runtime.sendMessage({
      request: 'updateDebugOptions', debugOpions: DEBUG
    });
  }
});
