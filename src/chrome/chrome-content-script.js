import XRegExp from 'xregexp';
/**
 * This file is an attempt to make the extension work on chrome.
 * For some obscure reasons, all the regexps disappear on chrome when passed in the promise.
 * The only solution I've found so far was to generate the regexp in the chrome-content-script
 * and pass it through the window element so I don't need to pass it via
 * promise anymore.
 */

/**
 * generate the regexps array and their replace word
 * @method generateReplaceEachRegexpsArray
 * @return {array}             An array of regexps and their replace word
 */
 function generateReplaceEachRegexpsArray() {
  const wordFilters = {
    mien: ['miem', 'miæn'],
    tous: ['touste', 'toustes', 'touxe', 'touxes'],
    celui: ['cellui', 'célui'],
    ceux: ['celleux', 'ceulles', 'ceuxes', 'ceuze'],
    eux: ['elleux', 'euxes', /*'euze', 'eus'*/],
    //mon: ['maon'],
    //ton: [/*'tan',*/ 'taon'],
    //son: ['saon'],
    ce: ['cès', 'cèx'],
    un: [/*'um',*/ 'unæ'],
    //du: ['di'],
    //le: ['lia'/*, 'li', 'lo'*/],
    ils: ['illes', 'ils\\.elles', 'ils·elles', 'elles\\.ils', 'elles·ils', 'il\\.elle\\.s', 'il·elle·s', 'elle\\.il\\.s', 'elle·il·s', /*'uls', 'ols', 'yels',*/ 'iels', 'ielles', /*'ims', 'iems', 'euls', 'yas'*/],
    il: ['ille', 'il\\.elle', 'il·elle', 'elle\\.il', 'elle·il', /*'ul', 'ol', 'yel',*/ 'iel', /*'el',*/ 'ielle', /*'im', 'iem', 'eul', 'ya', 'um', 'om', 'ax', 'ox'*/],
    lui: ['ellui']
  };

  let generatedWordsRegexp = [];
  const sBoundary = '(?<=^|\\P{Latin})';
  const eBoundary = '(?!-)(?=\\P{Latin}|$)';

  for(const [replaceWith, wordsToReplace] of Object.entries(wordFilters)) {
    let regexpStr = '';
    let i = 0;
    for(const word of wordsToReplace) {
      i++;
      regexpStr += sBoundary + word + eBoundary;
      regexpStr += i === wordsToReplace.length ? '' : '|';
    }
    generatedWordsRegexp[replaceWith] = XRegExp(regexpStr, 'mgiu');
  }

  return [
    [/s⋅e⋅s|s⋅es|s·e·s|s·es|s-e-s|s\.e\.s|s\.es/mgiu, 's'],
    [generatedWordsRegexp['mien'], 'mien'],
    [generatedWordsRegexp['tous'], 'tous'],
    [generatedWordsRegexp['celui'], 'celui'],
    [generatedWordsRegexp['ceux'], 'ceux'],
    [generatedWordsRegexp['eux'], 'eux'],
    // [generatedWordsRegexp['mon'], 'mon'],
    // [generatedWordsRegexp['ton'], 'ton'],
    // [generatedWordsRegexp['son'], 'son'],
    [generatedWordsRegexp['ce'], 'ce'],
    [generatedWordsRegexp['un'], 'un'],
    // [generatedWordsRegexp['du'], 'du'],
    // [generatedWordsRegexp['le'], 'le'],
    [generatedWordsRegexp['ils'], 'ils'],
    [generatedWordsRegexp['il'], 'il'],
    [generatedWordsRegexp['lui'], 'lui'],
    [XRegExp('(gentil(-|⋅|·|.)le|gentils(-|⋅|·|\\.)le)(-|⋅|·|\\.|)(?<!s|\\.(?=\\P{Latin}|$))', 'mgiu'), 'gentil'],
    [XRegExp('copaine', 'mgiu'), 'copain'],
    [/e-lle-au|e-au-lle|eau-elle|e⋅lle⋅au|e⋅au⋅lle|eau⋅elle|e·lle·au|e·au·lle|eau·elle|e\.lle\.au|e\.au\.lle|eau\.elle/mgiu, 'eau'],
    [/e-lles-aux|e-aux-lles|eaux-elles|e⋅lles⋅aux|e⋅aux⋅lles|eaux⋅elles|e·lles·aux|e·aux·lles|eaux·elles|e\.lles\.aux|e\.aux\.lles|eaux\.elles/mgiu, 'eaux'],
    [/-se-x|-ses-x|-x-se|-x-ses|⋅se⋅x|⋅ses⋅x|⋅x⋅se|⋅x⋅ses|·se·x|·ses·x|·x·se|·x·ses|\.se\.x|\.ses\.x|\.x\.se|\.x\.ses/mgiu, 'x'],
    [/-r-se|-se-r|-rs-se|-ses-r|⋅r⋅se|⋅se⋅r|⋅rs⋅se|⋅ses⋅r|·r·se|·se·r|·rs·se|·ses·r|\.r\.se|\.se\.r|\.rs\.se|\.ses\.r/mgiu, 'r'],
    [XRegExp('eurice(?=\\P{Latin}|s|$)', 'mgiu'), 'eur'],
    //Point médian
    [XRegExp('(?<=\\p{Latin})(·|⋅)[·⋅A-Za-zÀ-ÿ]*(?<!s)', 'mgiu'), ''],
    //Point
    [XRegExp('(?<!^|www)(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org)(?=\\P{Latin}|$))\\.(?=e\\.s|e(?=\\P{Latin}|$)|le|fe|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|le(?=\\P{Latin}|$)|te|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[.A-Za-zÀ-ÿ]*(?<!s|au\\W|x|\\.(?=\\P{Latin}|$))', 'mgiu'), ''],
    //Tiret
    [XRegExp('-(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org)(?=\\P{Latin}|$))(?=e-|e(?=\\P{Latin}|$)|fe-s(?=\\P{Latin}|$)|fe(?=\\P{Latin}|$)|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|te-s(?=\\P{Latin}|$)|te(?=\\P{Latin}|$)|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[-A-Za-zÀ-ÿ]*(?<!s|au\\W|x)', 'mgiu'), '']
  ];
}
window.replaceEachRegexps = generateReplaceEachRegexpsArray();
