import XRegExp from 'xregexp';

var replaceEachRegexps, bodyObserver;
var isExtensionEnabled = true;

var DEBUG = { log: false, timer: false };

browser.runtime.onMessage.addListener(
  (data, sender) => {
    if (data.type === 'debug') {
      DEBUG = data.content;
    }
  }
);

var timer = function(name) {
    let start = new Date();
    return {
        stop: function() {
            let end  = new Date();
            let time = end.getTime() - start.getTime();
            console.log('Timer:', name, 'finished in', time, 'ms');
        }
    }
};

/**
 * retrieve data from background script
 * @method requestContentScriptDataFromBackground
 * @return {Promise}
 */
function requestContentScriptDataFromBackground() {
  return browser.runtime.sendMessage({
    request: 'contentScriptData'
  }).then(
    async response => {
      replaceEachRegexps = await response.replaceEachRegexps;
      isExtensionEnabled = await response.isExtensionEnabled;
      DEBUG = await response.debugOpions;
    },
    error => console.error('Error retrieving replaceEachRegexps:', error)
  );
}

/**
 * Find the text nodes under a node and return them as an array
 * @method textNodesUnder
 * @param  {Node}       node The node to search text nodes in
 * @return {Array}           An array of the text nodes under the node parameter
 */
function textNodesUnder(node) {
  let n, a=[], walk=document.createTreeWalker(
    node,
    NodeFilter.SHOW_TEXT,
    {
      acceptNode: node => acceptNodeFilter(node)
    },
    false);
  while(n=walk.nextNode()) a.push(n);
  return a;
}

/**
 * Check if the text parentNode is not in nodesToReject or nodesToSkip
 * @method acceptNodeFilter
 * @param  {Node}         node The node to check
 * @return {unsigned short}
 */
function acceptNodeFilter(node) {
  const nodesToReject = ['svg', 'script', 'style', 'noscript', 'iframe', 'input'];
  const nodesToSkip = [/*'div'*/];

  // DEBUG: nodesArr[node.nodeName] = node.nodeName in nodesArr ? nodesArr[node.nodeName] + 1 : 1;
  if(nodesToReject.indexOf(node.parentNode.nodeName.toLowerCase()) > -1) {
    return NodeFilter.FILTER_REJECT;
  }

  if(isParentContentEditable(node)) {
    return NodeFilter.FILTER_REJECT;
  }

  if(nodesToSkip.indexOf(node.parentNode.nodeName.toLowerCase()) > -1) {
    return NodeFilter.FILTER_SKIP
  }

  return NodeFilter.FILTER_ACCEPT;
}

function isParentContentEditable(node) {
    let contenteditableAttributeFound = false;
    node.parentNode.childNodes.forEach(function (child) {
        if(child.nodeType != Node.ATTRIBUTE_NODE) return;
        if(child.nodeName != "contenteditable") return;
        if(child.nodeValue == "true" || child.nodeValue == "plaintext-only") contenteditableAttributeFound = true;
    });

    return contenteditableAttributeFound;
}

/**
 * Use the regexps to replace each occurrence in a node's textContent
 * @method fixTextNode
 * @param  {Text}    textNode
 */
function fixTextNode(textNode) {
  // DEBUG.log && console.log("fixTextNode", textNode);
  let originalText = textNode.textContent;
  let fixedText = XRegExp.replaceEach(textNode.textContent, replaceEachRegexps);
  if(originalText === fixedText) {
    return;
  }
  textNode.textContent = fixedText;
  DEBUG.log && console.log(
    {
      original: originalText,
      fixed: fixedText,
      node: textNode
    }
  );
}

/**
 * Retrieves the text nodes under a node calling textNodesUnder()
 * Then, foreach text node, call fixTextNode()
 * @method fixTextsUnderNode
 * @param  {Node}          node
 */
function fixTextsUnderNode(node) {
  let textNodesUnderTimer = DEBUG.timer ? timer('textNodesUnder()') : undefined;
  let pageTextNodes = textNodesUnder(node);
  DEBUG.timer && textNodesUnderTimer.stop();

  let foreachFixTextTimer = DEBUG.timer ? timer('foreachFixText') : undefined;
  pageTextNodes.forEach(textNode => fixTextNode(textNode));
  DEBUG.timer && foreachFixTextTimer.stop();
}

/**
 * Check the type of a mutationRecord
 * If the type is 'childList', foreach modified/added node, call fixTextsUnderNode()
 * If the type is 'characterData', and if it passes the filter, call fixTextNode()
 * @method processMutationRecord
 * @param  {MutationRecord}              mutationRecord is a Dom mutation
 */
function processMutationRecord(mutationRecord) {
  switch (mutationRecord.type) {
    case 'childList':
      mutationRecord.addedNodes.forEach(node => fixTextsUnderNode(node));
      break;

    case 'characterData':
      if(acceptNodeFilter(mutationRecord.target) === NodeFilter.FILTER_ACCEPT) {
        fixTextNode(mutationRecord.target);
      }
      break;
  }
}

function setBodyObserver() {
  bodyObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach((mutationRecord) => {
      processMutationRecord(mutationRecord);
    });
  });
  bodyObserver.observe(document.body, {attributes: false, childList: true, subtree:	true, characterData: true});
}

window.addEventListener('DOMContentLoaded', event => {
  requestContentScriptDataFromBackground().then(
    () => {
      //this is a hack to pass the array for chrome since it is bugged via promise
      //window.replaceEachRegexps would be defined in chrome-content-script.js and loaded before this content-script.js
      replaceEachRegexps = window.replaceEachRegexps ?? replaceEachRegexps;

      if(isExtensionEnabled) {
        fixTextsUnderNode(document.body);
        setBodyObserver();
      }
    }
  );
});
